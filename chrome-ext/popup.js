function PopupCtrl($scope, $window, $log) {
    $log.log('PopupCtrl init');
    
    $scope.hideMessage = '';
    $scope.hasArtifacts = false;
    
    
    $scope.toggleInspect = function() {
        $log.log('toggleInspect');
        getCurrentTab(function(tab) {
            chrome.tabs.sendMessage(tab.id, { signal: 'toggle-inspect' });
        });
    }

    $scope.newScrawl = function() {
        $log.log('newScrawl');
        getCurrentTab(function(tab) {
            chrome.tabs.sendMessage(tab.id, { signal: 'new-scrawl' });
        });
        $window.close();
    }

    $scope.signIn = function(email, password) {
        $log.log('auth request');
        chrome.runtime.sendMessage({ signal: 'sign-in', email: email, password: password }, function(resp) {
            $log.log('auth response', resp);
        });
    }
    
    $scope.toggleHideArtifacts = function () {
         $log.log('toggleHideArtifacts');
         getCurrentTab(function(tab) {
            chrome.tabs.sendMessage(tab.id, { signal: 'toggle-hide-artifacts' });
        });
        $window.close();
        
    }

    getCurrentTab(function(tab) {
        chrome.tabs.sendMessage(tab.id, { signal: 'get-inspect' }, function(resp) {
            $scope.$apply(function() {
                $scope.inspecting = resp.inspecting;
            });
        });
    });
}

