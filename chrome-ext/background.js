    if (true) {
        var log = function() { console.log.apply(console, arguments); }
    }
    else {
        var log = function() {}
    }


    function indicateDomIsAltered(isAltered, altsVisible) {
        console.log('signal received dom is alt:' + isAltered + altsVisible);
        if (isAltered === true) {
            if (altsVisible === true) {
                chrome.browserAction.setIcon({path: 'spraycan-login.png'});
            }
            else{
                chrome.browserAction.setIcon({path: 'spraycan-inactive.png'});
            }
        }
        else{
            chrome.browserAction.setIcon({path: 'splat.png'});
        }
    }

    
    log('loading background script');
    var BASE_URL = 'https://stickyfeeds.firebaseio.com';

    function contextMenuClick(info, tab) {
        chrome.tabs.sendMessage(tab.id, {signal:'newitem'});
    }

    chrome.contextMenus.create({"title": "Deface", "contexts": ["page", "image", "video"], "onclick": contextMenuClick});

    function notifyDraw(snap, url) {
        var o = snap.val();
        o.name = snap.name();
        log('got value from firebase', o);
        chrome.tabs.query({ url: url }, function(tabs) {
            for (var i = 0; i < tabs.length; i++) {
                log('sending artifact message to tab ' + tabs[i].id);
                chrome.tabs.sendMessage(tabs[i].id, { signal: 'artifact', data: o });
            }
        });
    }

    function notifyRemove(snap, url) {
        var name = snap.name();
        log('removing ' + name);
        chrome.tabs.query({ url: url }, function(tabs) {
            for (var i = 0; i < tabs.length; i++) {
                chrome.tabs.sendMessage(tabs[0].id, { signal: 'remove', name: name });
            }
        });
    }
    
    chrome.tabs.onActivated.addListener(function (a, b) {
        //check-dom-is-alterted
        console.log(a);
        console.log(a.tabId);
        chrome.tabs.sendMessage(a.tabId, { signal: 'check-dom-is-alterted'});
    });

    
    

    function loadItems(forUrl, sendResponse) {
        console.log('chtest');
        console.log('target is:'+forUrl);
        var target = hex_sha256(forUrl);
        var url = BASE_URL + '/uris/' + target;
        
        console.log(url);

        log('looking for items on ' + forUrl + ' (' + target + ')');
        var dataRef = new Firebase(url);
        dataRef.on('child_added', function(snap) {
            log('child_added');
            notifyDraw(snap, forUrl);
        });
        dataRef.on('child_changed', function(snap) {
            log('child_changed');
            notifyDraw(snap, forUrl);
        });
        dataRef.on('child_removed', function(snap) {
            log('child_removed');
            notifyRemove(snap, forUrl);
        });
    }

    function storeItem(forUrl, artifact, sendResponse) {
        var target = hex_sha256(forUrl);
        var url = BASE_URL + '/uris/' + target;

        log('storing item on ' + forUrl);

        if (!artifact.isNew) {
            url += '/' + artifact.name;
            log('updating artifact ' + artifact.name + ': ' + url);
            var dataRef = new Firebase(url);
            dataRef.update({ data: artifact.data, coords: artifact.coords });
            sendResponse(null);
        }
        else {
            var dataRef = new Firebase(url);
            doc = {
                coords: artifact.coords,
                data: artifact.data,
                ts: (new Date()).toJSON()
            }
            var newRef = dataRef.push(doc);
            sendResponse({ 'name': newRef.name() });
        }
    }
    
    function delItem(forUrl, artifact, sendResponse) {
        var target = hex_sha256(forUrl);
        var url = BASE_URL + '/uris/' + target;
        
        log('deleting item on ' + forUrl);
        
        url += '/' + artifact.name;
        //log('updating artifact ' + artifact.name + ': ' + url);
        var dataRef = new Firebase(url);
        dataRef.remove();
        sendResponse(null);
    }

    function signIn(email, password, sendResponse) {
        var authref = new Firebase(BASE_URL);
        var auth = new FirebaseSimpleLogin(authref, function(error, user) {
            var u;
            if (error) {
                log('auth error', error);
                u = false;
            }
            else {
                log('auth user', user);
                u = user
            }
            sendResponse({ user: u });
        });
        auth.login('password', { email: email, password: password });
        return true;
    }

    function shareViaEmail(uri, payload){
        console.log('about to attempt ajax post');
        console.log($);
        $.ajax({
            type: "POST",
            url: 'http://api.stickyfeeds.com/raw-uris/' + encodeURIComponent(uri) + '/email',
            data: JSON.stringify(payload),
            success: function(){
                console.log('success');
                getCurrentTab(function(tab) {
                    chrome.tabs.sendMessage(tab.id, { signal: 'toggle-share-dialog' });
                });
            },
            error: function(e){console.log('there was an error'); console.log(e);},
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        });
    }

    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {
            switch (request.signal) {
                case 'get':
                    loadItems(sender.tab.url);
                    break;
                case 'load-image':
                    loadImage(request.id);
                    break;
                case 'store':
                    storeItem(sender.tab.url, request.artifact, sendResponse);
                    break;
                case 'del':
                    delItem(sender.tab.url, request.artifact, sendResponse);
                    break;
                case 'sign-in':
                    log('background received sign in signal');
                    signIn(request.email, request.password, sendResponse);
                    break;
                case 'dom-is-altered':
                    log('dom is altered');
                    indicateDomIsAltered(true, true);
                    break;
                case 'dom-is-not-altered':
                    log('dom is clean')
                    indicateDomIsAltered(false, false);
                    break;
                case 'dom-alterations-hidden':
                    log('dom is clean')
                    indicateDomIsAltered(true, false);
                    break;
                case 'share-via-email':
                    log('share via email')
                    shareViaEmail(request.uri, request.payload);
                    break;
            }
            return true;
        }
    );
