var getCurrentTab = function(callback) {
    chrome.tabs.query(
        { windowId: chrome.windows.WINDOW_ID_CURRENT, active: true }, 
        function(tabs) { callback(tabs[0]); }
    );
};

