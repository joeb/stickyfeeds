window.bridge = (function () {
	return {
		publish: chrome.runtime.sendMessage, // sendMessage
		subscribe: function (fn) {
			chrome.runtime.onMessage.addListener(fn); // onMessage.addListener
		},
		getUrl: chrome.extension.getURL
	};
}());
