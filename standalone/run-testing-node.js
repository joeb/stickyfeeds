/*jshint devel: true, indent: 4, node:true*/
'use strict';

//Run server component
require('../server/api-server.js');

var st = require('node-static');
var headers = {
    "Access-Control-Allow-Origin":"*",
    "Access-Control-Allow-Credentials": "true",
    "Access-Control-Allow-Methods": "OPTIONS, GET, POST, PUT, DELETE",
    "Access-Control-Allow-Headers": "Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control"
};

var file = new st.Server('.', { cache: 7200, headers: headers });

//Serving files needed by phantomjs on loopback
require('http').createServer(function (request, response) {
    file.serve(request, response);
}).listen(8888, 'localhost', function () {
        console.log('Serving standalone pages on localhost:8888');

    });




