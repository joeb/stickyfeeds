window.bridge = (function () {
	function dummy(arg) {
		console.log(arguments)
		return arg;
	}
	return {
		publish: dummy, // sendMessage
		subscribe: dummy, // onMessage.addListener
		getUrl: dummy
	};
}());
