/*jshint devel: true, indent: 4, node:true*/
'use strict';

var restify = require('restify');
var spn = require('./sticky-phantom-node.js');
var st = require('node-static');

var file = new st.Server('.', { cache: 7200, headers: {'X-Hello':'World!'} });

//Serving files needed by phantomjs on loopback
require('http').createServer(function (request, response) {
    file.serve(request, response);
}).listen(8000, 'localhost', function () {

    console.log('serving files on localhost:8000');
    console.log('Example uri: http://localhost:8080/raw-uris/http%3A%2F%2Fchc.name%2F/sticky-screenshot/300');

    function stickyScreenshot(req, res, next) {
        //Start mussin!
        var a = spn();
        a.renderDelay = 1000;
        a.screenshotPath = '/tmp/';
        a.windowX = 1024;
        a.windowY = 768;
        a.screenshotX = req.params.width;
        a.stickyScreenshot(req.params.uri)
        .then(function (data) { res.setHeader('Content-Type', 'image/png'); res.end(new Buffer(data, "base64")); });
    }

    function cleanScreenshot(req, res, next) {
        //Start mussin!
        var a = spn();
        a.renderDelay = 500;
        a.screenshotPath = '/tmp/';
        a.windowX = 1024;
        a.windowY = 768;
        a.screenshotX = req.params.width;
        a.cleanScreenshot(req.params.uri)
            .then(function (data) { res.setHeader('Content-Type', 'image/png'); res.end(new Buffer(data, "base64")); });
    }

    var server = restify.createServer();
    server.get('/raw-uris/:uri/sticky-screenshot/:width', stickyScreenshot);
    server.get('/raw-uris/:uri/clean-screenshot/:width', cleanScreenshot);

    server.listen(8080, function() {
        console.log('%s listening at %s', server.name, server.url);
    });


});