window.bridge = (function () {
	var callbacks = jQuery.Callbacks();

	// default logging subscriber for all topics
	callbacks.add(function (message, callback) { 
		console.log('Test bridge got message with signal "' + message.signal + ':"');
		console.log(message);
		console.log(callback);
	});

	return {
		publish: callbacks.fire, // sendMessage
		subscribe: function (fn) {
			callbacks.add(function (arg) {
				fn(arg, null,  function () { console.log(arguments); /* TODO sendResponse */ });
			});
		},
		getUrl: function (arg) { return arg; }
	};

}());
