/*jshint devel: true, indent: 4, node:true*/
'use strict';

var restify = require('restify');
var spn = require('./sticky-phantom-node.js');
var st = require('node-static');
var mandrill = require('mandrill-api/mandrill');
var _ = require('underscore');

var file = new st.Server('.', { cache: 7200, headers: {'X-Hello':'World!'} });

//Serving files needed by phantomjs on loopback
require('http').createServer(function (request, response) {
    file.serve(request, response);
}).listen(8000, 'localhost', function () {

    console.log('Serving static files for Phantom API component on localhost:8000');
    console.log('Example uri: http://localhost:8080/raw-uris/http%3A%2F%2Fchc.name%2F/sticky-screenshot/300');

    function stickyScreenshot(req, res, next) {
        //Start mussin!
        var a = spn();
        a.renderDelay = 2000;
        a.screenshotPath = '/tmp/';
        a.windowX = 1024;
        a.windowY = 768;
        a.screenshotX = req.params.width;
        a.stickyScreenshot(req.params.uri)
        .then(function (data) { res.setHeader('Content-Type', 'image/png'); res.end(new Buffer(data, "base64")); });
    }

    function cleanScreenshot(req, res, next) {
        //Start mussin!
        var a = spn();
        a.renderDelay = 500;
        a.screenshotPath = '/tmp/';
        a.windowX = 1024;
        a.windowY = 768;
        a.screenshotX = req.params.width;
        a.cleanScreenshot(req.params.uri)
            .then(function (data) { res.setHeader('Content-Type', 'image/png'); res.end(new Buffer(data, "base64")); });
    }

    function sendEmail (req, res, next){
        res.header("Access-Control-Allow-Origin", "*");

        var sender = req.body.sender;
        var shareUri = req.params.uri;

        var toVar = [];

        _.each(req.body.recipients, function(emailAddr){
            toVar.push({
                "email": emailAddr,
                "name": "",
                "type": "to"
            });
        });


        var mandrill_client = new mandrill.Mandrill('m_GJJiLaJBOrkrRodLOy_w');

        //Todo: insecure use of client header
        var screenshotHost = req.headers.host;

        //Templating email html
        _.templateSettings = {
            interpolate: /\{\{(.+?)\}\}/g
        };



        var template = _.template('\
            <h1>{{sender}} wants to feed you something sticky.</h1>\n\
            <h2>Check out this page:{{shareUri}}</h2>\n\
            <div>\n\
              <p>Sticky:</p> <img src="http://{{screenshotHost}}/raw-uris/{{shareUriEnc}}/sticky-screenshot/300" alt="some_text">\n\
              <p>Not sticky:</p> <img src="http://{{screenshotHost}}/raw-uris/{{shareUriEnc}}/clean-screenshot/300" alt="some_text">\n\
            </div>'
        );

        var htmlMessage = template({
            screenshotHost:screenshotHost,
            sender:sender,
            shareUriEnc:encodeURIComponent(shareUri),
            shareUri:shareUri
        });

        //console.log(htmlMessage);


        var message = {
            "html": htmlMessage,
            "text": "How sticky is your web?",
            "subject": "How sticky is your web?",
            "from_email": "info@stickyfeeds.com",
            "from_name": "StickyFeeds",
            "to": toVar,
            "headers": {
                "Reply-To": sender
            }
        };

        console.log(req.query);


        var sendTheEmail;

        if(_.has(req.query, 'sendTheEmail')){
            sendTheEmail = (req.query.sendTheEmail === true);
        }
        else{
            sendTheEmail = true;
        }

        console.log(message);
        if(sendTheEmail === true){
            var async = true;
            console.log('sending email');

            mandrill_client.messages.send({"message": message, "async": async}, function(result) {
                console.log('result:');
                console.log(result);
            }, function(e) {
                console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
            });
        }
        else{
            console.log('don\'t send activated, not sending');
        }

        res.send('done');
    }




    var server = restify.createServer();

    server.use(restify.acceptParser(server.acceptable));
    server.use(restify.queryParser());
    server.use(restify.bodyParser());

    server.get('/raw-uris/:uri/sticky-screenshot/:width', stickyScreenshot);
    server.get('/raw-uris/:uri/clean-screenshot/:width', cleanScreenshot);
    server.post('/raw-uris/:uri/email', sendEmail);

    server.listen(8080, function() {
        console.log('API listening at %s', server.url);
    });


});
