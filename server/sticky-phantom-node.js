/*jshint devel: true, indent: 4, node:true*/
/*

TODO: Should make fpath if it doesn't exist
 */
"use strict";

console.log('dir name is:' + __dirname);

var phantom = require('phantom'); //The most used phantom lib
var Q = require('q');
var request = require('request'); //High-level http lib
var crypto = require('crypto');
var BASE_URL = 'https://stickyfeeds.firebaseio.com';
var gm = require('gm'); //GraphicsMagick
var fs = require('fs');
var ph; //Phantom Instance, singleton, will be set to false, then asynchronously set to an instance

function setPh() {
    //Singleton for Phantom instance to asynchronously handle concurrency
    var deferred = Q.defer();

    function handleFalse() {
        //This function is activated if another event is already setting up phantom but it has not completed
        if (ph) {
            //Phantom instance is set/already set and ready to handle requests
            deferred.resolve();
        }
        else if (ph === false) {
            setTimeout(handleFalse, 100);
        }
        else {
            //In this function ph should only ever be truthy or false.
            process.exit(1);
        }
    }

    if (ph) {
        //Phantom is already defined, resolve ASAP
        deferred.resolve();
    }
    else if (ph === undefined) {
        //Setting to false to synchronously prevent other events from trying to create a new instance
        ph = false;
        phantom.create('--web-security=false', function (p) {
            ph = p;
            deferred.resolve();
        });
    }
    else {
        handleFalse();
    }

    return deferred.promise;
}


function createPage(width, height) {
    var deferred = Q.defer();

    if (ph === undefined || ph === false) {
        //createPage should never be called if ph isn't set, die straight away
        process.exit(1);
    }

    ph.createPage(function (page) {
        page.set('viewportSize', {width: width, height: height}, function (test) {
            console.log('Page created');
            deferred.resolve(page);
        });
    });

    return deferred.promise;
}


function openUrl(page, url) {
    var deferred = Q.defer();

    page.open(url, function (status) {
        deferred.resolve(page);
    });

    return deferred.promise;
}


function qInject(page, scriptPath) {
    var deferred = Q.defer();

    page.injectJs(scriptPath, function (status) {
        console.log('Done injecting script:' + scriptPath);
        deferred.resolve(page);
    });

    return deferred.promise;
}

function qEvalEnhanced(page, fn) {
    var deferred = Q.defer(),
        args = [].slice.call(arguments, 2),
        functionToParse;

    if (args) {
        //If arguments were provided we need to use apply and stringify them
        functionToParse = "function() { return (" + fn.toString() + ").apply(this, " + JSON.stringify(args) + ");}";
    }
    else {
        functionToParse = fn;
    }

    page.evaluate(functionToParse, function (result) {
        console.log('Evaluated code, result:' + result);
        deferred.resolve(page);
    });

    return deferred.promise;
}

function enforceDimensions(x, y) {
    document.body.style.width = x;
    document.body.style.height = y;
    return true;
}


function phantomLoadCss() {
    /*global $ */
    $("head").append("<link id='some sticky css' href='http://localhost:8000/sf/sf.css' type='text/css' rel='stylesheet' />");
    var a = $("head link");
    return a[a.length - 1].outerHTML;
}

function qScreenshot(page, fpath) {

    //Collision resistant file name
    var fname = (new Date()).getTime().toString() + Math.random().toString().substring(2, 12) + '.png',
        deferred = Q.defer();

    page.render(fpath + fname, function () {
        console.log('Rendered a screenshot');
        deferred.resolve(fpath + fname);
    });

    return deferred.promise;
}

function qPageSet(page, prop, val) {
    var deferred = Q.defer();

    page.set(prop, val, function () {
        deferred.resolve(page);
    });

    return deferred.promise;
}

function pushArtifacts(artifacts) {
    /*global bridge*/
    var i;

    for (i = 0; i < artifacts.length; i++) {
        bridge.publish({signal: 'artifact', data: artifacts[i]});
    }
}

function qDelayPage(page, delayTime) {
    var deferred = Q.defer();

    console.log('Delaying phantom for ' + delayTime);

    setTimeout(function () {
        console.log('Delay completed');
        deferred.resolve(page);
    }, delayTime);

    return deferred.promise;
}

function qResize(fpath, xSize, ySize) {
    var deferred = Q.defer();

    gm(fpath).resize(xSize).write(fpath, function () {
        deferred.resolve(fpath);
    });

    return deferred.promise;
}

function qReadFile(handle){
    var deferred = Q.defer();

    fs.readFile(handle, 'base64', function (err,data) {
        if (err) {
            deferred.reject(new Error(err));
        }
        else{
            deferred.resolve(data);
        }
    });

    return deferred.promise;
}


function stickyFactory() {
    var r = {};
    r.windowX = 1024;
    r.windowY = 768;
    r.screenshotX = 600;
    r.screenshotY = 400;

    r.url = undefined;
    r.renderDelay = 3000; //default delay in miliseconds

    r.screenshotPath = './';

    r.getData = function (page, url) {
        console.log('get data url is:' + url);
        var deferred = Q.defer(),
            i = 0,
            retArray = [],
            fullUrl,
            shasum = crypto.createHash('sha256');

        shasum.update(url);
        fullUrl = BASE_URL + '/uris/' + shasum.digest('hex') + '.json';

        console.log('Initiating request for:' + fullUrl);

        request(fullUrl, function (error, response, body) {
            //console.log('parsing data');
            var data = JSON.parse(body),
                property;
            //console.log('done parsing data');
            //console.log(data);

            for (property in data) {
                if (data.hasOwnProperty(property)) {
                    retArray.push({});
                    //console.log('\tprop is:' + property);
                    retArray[i].name = property;
                    retArray[i].data = data[property].data;
                    retArray[i].coords = data[property].coords;
                    i++;
                }
            }

            deferred.resolve({page:page, artifacts: retArray});
        });

        return deferred.promise;
    };

    r.cleanScreenshot = function (url) {
        return setPh()
        .then(function () {return createPage(r.windowX, r.windowY); })
        //.then(function(page){return qPageSet('zoomFactor', .5);})
        .then(function (page) {return qPageSet(page, 'settings.userAgent',
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36'); })
        .then(function (page) {return openUrl(page, url); })
        .then(function (page) {return qEvalEnhanced(page, enforceDimensions, r.windowX, r.windowY); })
        .then(function (page) {return qDelayPage(page, r.renderDelay); })
        .then(function (page) {return qScreenshot(page, r.screenshotPath); })
        .then(function (fpath) {return qResize(fpath, r.screenshotX); })
        .then(function (fpath) {return qReadFile(fpath); });
    };

    r.stickyScreenshot = function (url) {
        console.log('get modified.');
        return setPh()
        .then(function () {return createPage(r.windowX, r.windowY); })
        //.then(function(page){return qPageSet('zoomFactor', .5);})
        .then(function (page) {return qPageSet(page, 'settings.userAgent',
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36'); })
        .then(function (page) {return openUrl(page, url); })
        .then(function (page) {return qInject(page, __dirname + '/vendor/jquery.js'); })
        .then(function (page) {return qEvalEnhanced(page, phantomLoadCss); })
        .then(function (page) {return qInject(page, __dirname + '/vendor/firebase.js'); })
        .then(function (page) {return qInject(page, __dirname + '/vendor/angular.js'); })
        .then(function (page) {return qInject(page, __dirname + '/vendor/hammer.min.js'); })
        .then(function (page) {return qInject(page, __dirname + '/server-bridge.js'); })
        .then(function (page) {return qInject(page, __dirname + '/sf/sf.js'); })
        .then(function (page) {return r.getData(page, url); })
        .then(function (payload) {return qEvalEnhanced(payload.page, pushArtifacts, payload.artifacts); })
        .then(function (page) {return qEvalEnhanced(page, enforceDimensions, r.windowX, r.windowY); })
        .then(function (page) {return qDelayPage(page, r.renderDelay); })
        .then(function (page) {return qScreenshot(page, r.screenshotPath); })
        .then(function (fpath) {return qResize(fpath, r.screenshotX); })
        .then(function (fpath) {return qReadFile(fpath); });
    };

    return r;
}

module.exports = stickyFactory;