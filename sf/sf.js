(function($) {
    console.log('StickyFeeds is sticky');
	
	function drawingBoardFill(ctx, coords, fColor, mCoords) {
		//This function is borrowed directly from drawingboardjs
		//http://leimi.github.io/drawingboard.js/
		//This should probably be broken out into a separate file
		
		var DrawingBoard = {};
		DrawingBoard.Utils = {};
		this.ctx = ctx;
		this.ctx.strokeStyle = fColor;
		this.canvas = coords;
		var e = {}; //todo: what is this supposed to be???
		e.coords = mCoords;
		
		DrawingBoard.Utils.RGBToInt = function(r, g, b, a) {
			var c = 0;
			c |= (r & 255) << 24;
			c |= (g & 255) << 16;
			c |= (b & 255) << 8;
			c |= (a & 255);
			return c;
		};
		
		DrawingBoard.Utils.pixelAt = function(image, x, y) {
			var i = (y * image.width + x) * 4;
			var c = DrawingBoard.Utils.RGBToInt(
				image.data[i],
				image.data[i + 1],
				image.data[i + 2],
				image.data[i + 3]
			);
			
			var ret = [
				i, // INDEX
				x, // X
				y, // Y
				c  // COLOR
			];
			
			return ret;
		};
		
		var img = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
		
		// constants identifying pixels components
		var INDEX = 0, X = 1, Y = 2, COLOR = 3;
		
		// target color components
		var stroke = this.ctx.strokeStyle;
		var r = parseInt(stroke.substr(1, 2), 16);
		var g = parseInt(stroke.substr(3, 2), 16);
		var b = parseInt(stroke.substr(5, 2), 16);
		//var a = parseInt(stroke.substr(7, 2), 16);
		var a = 255;
		
		// starting point
		var start = DrawingBoard.Utils.pixelAt(img, parseInt( e.coords.x, 10), parseInt( e.coords.y, 10));
		
		// no need to continue if starting and target colors are the same
		if (start[COLOR] === DrawingBoard.Utils.RGBToInt(r, g, b, a)){
			console.log('same color detection triggered');
			console.log('debug');
			return;
		}
		
		// pixels to evaluate
		var queue = [start];
		
		// loop vars
		var pixel, x, y;
		var maxX = img.width - 1;
		var maxY = img.height - 1;
		
		while ((pixel = queue.pop())) {
			
			if (pixel[COLOR] === start[COLOR]) {
				//Rewrite pixel to new color
				img.data[pixel[INDEX]] = r;
				img.data[pixel[INDEX] + 1] = g;
				img.data[pixel[INDEX] + 2] = b;
				img.data[pixel[INDEX] + 3] = a;
				
				if (pixel[X] > 0) // west
					queue.push(DrawingBoard.Utils.pixelAt(img, pixel[X] - 1, pixel[Y]));
				if (pixel[X] < maxX) // east
					queue.push(DrawingBoard.Utils.pixelAt(img, pixel[X] + 1, pixel[Y]));
				if (pixel[Y] > 0) // north
					queue.push(DrawingBoard.Utils.pixelAt(img, pixel[X], pixel[Y] - 1));
				if (pixel[Y] < maxY) // south
					queue.push(DrawingBoard.Utils.pixelAt(img, pixel[X], pixel[Y] + 1));
			}
		}
		this.ctx.putImageData(img, 0, 0);
	}

    function Artifact(o) {
		
		this.coords = o.coords;
		this.ts = o.ts;
		this.name = o.name;
		this.hide = false;
		
		this.undoStack = [];
		this.redoStack = [];
		this.currentCanvas = null;
		
		if (o.data) {
			this.undoStack.push(o.data);
			this.currentCanvas = o.data;
		}
    }
    
    Artifact.prototype.addUndoLayer = function (imageData) {
        // New canvas for drawing
        this.undoStack.push(imageData);
		// Purge the redo stack
		this.redoStack = [];
		this.currentCanvas = imageData;
    };
    
    Artifact.prototype.undo = function () {
		var p;
		p = this.undoStack.pop();
		//Make sure we don't push an undefined element onto the stack.
		if (p){
			this.redoStack.push(p);
			this.currentCanvas = this.undoStack[this.undoStack.length - 1];
		}
    };
    
    Artifact.prototype.redo = function () {
		var p;
		p = this.redoStack.pop();
		//Make sure we don't push an undefined element onto the stack.
		if (p){
			this.undoStack.push(p);
			this.currentCanvas = p;
		}
	};

    Artifact.prototype.getImageData = function () {
		return this.undoStack[this.undoStack.length - 1];
    }
	
    var MAX_AGE = 1000 * 60 * 60 * 12;
	
    function initAngular(data) {
        var root = $(data);
        root.appendTo(document.body);
		
        var app = angular.module('sf', []);

        app.config(function($sceProvider) {
            $sceProvider.enabled(false);
        });
		
        app.run(function($rootScope, $log) {
            $rootScope.inspecting = false;
            $rootScope.shareDialog = false;
            $rootScope.$watch('inspecting', function(n, o) {});
			
            bridge.subscribe(function(request, sender, sendResponse) {
                if (request.signal == 'toggle-inspect') {
                    $log.log('toggling inspect');
                    $rootScope.$apply(function() {
                        $rootScope.inspecting = !$rootScope.inspecting;
                    });
                }
                if (request.signal == 'toggle-share-dialog') {
                    $log.log('toggling share dialog:' + $rootScope.shareDialog);
                    $rootScope.$apply(function() {
                        $rootScope.shareDialog = !$rootScope.shareDialog;
                    });
                }
            });
        });
		
		app.factory('editorState', function(){
			
			var r = {};
			r.tool = 'pencil';
			r.brushWidth = 1;
			r.brushColor = '#000000';
			r.lineCap = 'round';
			r.activeArtifact = {};
			
			r.getCompositeType = function () {
				if (this.tool === 'pencil') {
					return 'source-over';
				}
				else if(this.tool === 'eraser') {
					return 'destination-out';
				}
				else if (this.tool === 'fillTool') {
					return 'source-over';
				}
				else{
					console.log('invalid tool type.');
					return null;
				}
			}
			
			return r;
		});
		
        app.factory('Artifacts', function($rootScope, $timeout, $log, editorState) {
            var seq = 0;
            var artifacts = {};
            var inspecting = false;
            var currentCanvas = '';
            var editStack = [];
			var hideToggle = false;
			
			function domIsAlterted() {
				if (Object.keys(artifacts).length === 0) {
					console.log('No artifacts for this page.')
					bridge.publish({signal: 'dom-is-not-altered'});
				}
				else{
					if (hideToggle === false ) {
						console.log('DOM is altered');
						bridge.publish({signal: 'dom-is-altered'});
					}
					else{
						console.log('DOM alternations are hidden');
						bridge.publish({signal: 'dom-alterations-hidden'});
					}	
				}
			}
			
			domIsAlterted();
			
            function save() {
                $log.log('Saving...');
				
                bridge.publish({
                    signal: 'store',
                    artifact: {
                        name: editorState.activeArtifact.name,
                        isNew: editorState.activeArtifact.isNew,
                        coords: editorState.activeArtifact.coords,
                        data: editorState.activeArtifact.currentCanvas
                    }
                }, function(resp) {
                        if (editorState.activeArtifact.isNew) {
                            remove(editorState.activeArtifact.name);
                        }
                });
            }
			
            //This function will add a new canvas to hold drawings
            function add(data, inspect) {
                var a = new Artifact(data);
                if (!('name' in data)) {
                    a.name = 'new-' + ++seq;
                    a.isNew = true;
                }
                a.inspecting = inspect;
                $rootScope.$apply(function() {
                    artifacts[a.name] = a;
                });
				domIsAlterted();
            }
			
            function createNew(data, inspect) {
                var a = new Artifact(data);
                if (!('name' in data)) {
                    a.name = 'new-' + ++seq;
                    a.isNew = true;
                
                    if(currentCanvas.hasOwnProperty("name")){
                        editStack.push(currentCanvas.name);
                    }
                    currentCanvas = a;
                }
				
                a.inspecting = inspect;
                $log.log('adding artifact', a);
				
                $rootScope.$apply(function() {
                    artifacts[a.name] = a;
                });
            }
			
            function remove(name) {
                delete artifacts[name];
				//Todo, add call to delete from server
				 bridge.publish({
                    signal: 'del',
                    artifact: {
                        name: editorState.activeArtifact.name,
                        isNew: editorState.activeArtifact.isNew,
                        coords: editorState.activeArtifact.coords,
                        data: editorState.activeArtifact.currentCanvas
                    }
				});
            }
			
			function hide() {
				console.log('hide triggered');
				hideToggle = !hideToggle;
				$rootScope.$apply(function() {
					for (a in artifacts) {
						artifacts[a].hide = hideToggle;
						console.log(artifacts[a])
					}	
				});
				domIsAlterted();
			}
			
            bridge.subscribe(function(request, sender, sendResponse) {
                switch (request.signal) {
                    case 'artifact':
                        var n = request.data.name;
                        add(request.data);
                        break;
                    case 'remove':
                        $log.log('got remove signal for ' + request.name);
                        remove(request.name);
                        break;
                    case 'get-inspect':
                        sendResponse({ inspecting: $rootScope.inspecting });
                        break;
					case 'toggle-hide-artifacts':
                        $log.log('hiding artifacts 123');
						hide();
                        break;
					case 'check-dom-is-alterted':
						$log.log('triggered dom is altered check');
						domIsAlterted();
						break;
                }
                return true;
            });
			
            bridge.publish({ signal: "get" });
			
            return { artifacts: artifacts, add: add, save: save, createNew: createNew, remove: remove, hide: hide};
        });
		
        app.controller('MainCtrl', function($scope, $log) {});
		
        app.controller('ArtifactCtrl', function($scope, $log, Artifacts) {
            $scope.artifacts = Artifacts.artifacts;
        });
		
        app.directive('placeholder', function($log, Artifacts) {
            return {
                restrict: 'E',
                replace: true,
                template: '<div id="sf-placeholder"></div>',
                scope: {},
                link: function(scope, element, attrs, ctrl) {
                    
                    var x0, y0;
                    var $D = $(document.createElement('div')).attr('id', 'sf-glass').appendTo(document.body);
					element = $(element);
					
                    bridge.subscribe(function(request, sender, sendResponse) {
                        switch (request.signal) {
                            case 'new-scrawl':
                                $log.log('got create signal');
                                $D.show();
                                Hammer($D[0]).on('dragstart', onmousedown);
                                Hammer($D[0]).on('dragend', onmouseup);
                                break;
                        }
                    });
					
                    function onmousemove(e) {
                        e.gesture.preventDefault();
                        e.gesture.stopPropagation();
						
                        var top = Math.min(e.gesture.center.pageY, y0);
                        var left = Math.min(e.gesture.center.pageX, x0);
                        var height = Math.abs(e.gesture.center.pageY - y0) + 1;
                        var width = Math.abs(e.gesture.center.pageX - x0) + 1;
						
                        element.css({ top: top, left: left, height: height, width: width });
                    }
					
                    function onmousedown(e) {
                        e.gesture.preventDefault();
                        e.gesture.stopPropagation();
                        Hammer($D[0]).on('drag', onmousemove);
                        x0 = e.gesture.center.pageX;
                        y0 = e.gesture.center.pageY;
                        element.css({ top: y0, left: x0, height: 0, width: 0 }).show();
                    }
					
                    //This function combined with the one above passes coordinate data to Artifacts to create new canvas
                    function onmouseup(e) {
                        e.gesture.preventDefault();
                        e.gesture.stopPropagation();
                        var c = element.offset();
                        element.hide();
                        $D.hide();
                        Artifacts.createNew({ coords: { top: c.top, left: c.left, height: element.height(), width: element.width() }}, true);
                    }
                    
                }
            };
        });
		
        app.directive('artifact', function($log, Artifacts, $rootScope, editorState) {
			
            return {
                restrict: 'E',
                replace: true,
                templateUrl: bridge.getUrl('sf/ng-templates/artifact.html'),
                scope: { model: '=' },
                link: function(scope, element, attrs, ctrl) {
                    var painting = false;
					var ctx = null;
					var coords = {}
					
					element = $(element);
					
					function setCtx() {
						ctx = $(element[0]).find("canvas").last()[0].getContext('2d');
					}
					
					function setActiveArtifact() {
						editorState.activeArtifact = scope.model;
					}
					
					var draw = {
						compositeType: 'source-over',
						start : function (e, that){
							
							ctx.lineWidth = editorState.brushWidth;
							ctx.strokeStyle = editorState.brushColor;
							ctx.lineCap = editorState.lineCap;
							ctx.globalCompositeOperation = this.compositeType;
							
							e.gesture.preventDefault();
							e.gesture.stopPropagation();
							painting = true;
							element.addClass('painting');
							ctx.beginPath();
							var mouseX = e.gesture.center.pageX - that.offsetLeft;
							var mouseY = e.gesture.center.pageY - that.offsetTop;
							ctx.moveTo(mouseX, mouseY);
						},
						carryOn : function (e, that) {
							if (painting) {
								e.gesture.preventDefault();
								e.gesture.stopPropagation();
								var mouseX = e.gesture.center.pageX - that.offsetLeft;
								var mouseY = e.gesture.center.pageY - that.offsetTop;
								ctx.lineTo(mouseX, mouseY);
								ctx.stroke();
							}
						},
						stop : function (e){
							e.gesture.preventDefault();
							e.gesture.stopPropagation();
							painting = false;
							element.removeClass('painting');
							ctx.save(2);
							
							//Make sure the canvas that was just drawn on is active so save will work.
							scope.$apply(function (){
								ctx.globalCompositeOperation = 'source-over';
								scope.model.addUndoLayer(ctx.canvas.toDataURL());
							});
						}
					}
					
					var erase = Object.create(draw);
					erase.compositeType = 'destination-out';
					
					function fill(e, that){
						coords.x = e.gesture.center.pageX - that.offsetLeft;
							coords.y = e.gesture.center.pageY - that.offsetTop;
							
							scope.$apply(function (){
								ctx.globalCompositeOperation = 'source-over';
								drawingBoardFill(ctx, scope.model.coords, editorState.brushColor, coords);
								scope.model.addUndoLayer(ctx.canvas.toDataURL());
							});
						
					}
					
					 Hammer(element[0]).on('tap', function(e) {
						setActiveArtifact();
						setCtx();
						if (editorState.tool === 'pencil') {
							//todo: draw single dot
						}
						else if (editorState.tool === 'fillTool') {
							fill(e, this);
						}
                    });
					
                    //Start drawing line on canvas
                    Hammer(element[0]).on('dragstart', function(e) {
						setActiveArtifact();
						setCtx();
						if (editorState.tool === 'pencil') {
							draw.start(e, this);
						}
						else if (editorState.tool === 'eraser') {
							erase.start(e, this);
						}
                    });
					
                    //Drawing on canvas
                    Hammer(element[0]).on('drag', function(e) {
						if (editorState.tool === 'pencil') {
							draw.carryOn(e, this);
						}
						else if (editorState.tool === 'eraser') {
							erase.carryOn(e, this);
						}
                    });
					
					//Done drawing on canvas
                    Hammer(element[0]).on('dragend', function(e) {
						if (editorState.tool === 'pencil') {
							draw.stop(e);
						}
						else if (editorState.tool === 'eraser') {
							erase.stop(e);
						}
					});
                }
            };
        });
		
		app.directive('sfImageData', function($log, $rootScope, editorState){
            /*
            ** draws a canvase from a png data URI from scope variable, e.g., <canvas sf-image-data="model.imageData">
            */
			return {
                restrict: 'A',
				scope:{
                    base64png: '=sfImageData',
                    height: '@',
                    width: '@'
				},
				link: function(scope, element, attrs, ctrl) {
					var canv = element[0];
					var ctx = canv.getContext('2d');
                    scope.$watch('base64png', function(val) {
						ctx.clearRect(0, 0, scope.width, scope.height);
						if (val) {
							var image = document.createElement('img');
							image.src = val;
							image.onload = function() {
								ctx.drawImage(image, 0, 0);
							};
						}
                    });
				}
			};
		});

        app.directive('emailform', function($log, $rootScope, editorState){
            return {
                restrict: 'E',
                replace: true,
                templateUrl: bridge.getUrl('sf/ng-templates/emailform.html'),
                scope: {},
                controller: function($scope, $log, $element, $http) {

                    $scope.sender = '';
                    $scope.recipients = [];

                    $scope.sendEmail = function () {
                        var payload = {};
                        payload.sender = $scope.sender;
                        payload.recipients = $scope.recipients.split(/[,\s]+/);

                        //bridge.getUrl(''),
                        //http://localhost:8080/raw-uris/http%3A%2F%2Fchc.name%2F/sticky-screenshot/300

                        //$http({method: 'POST', url: 'http://localhost:8080/raw-uris/http%3A%2F%2Fchc.name%2F/email'}).

                        bridge.publish({
                                signal: 'share-via-email',
                                uri: $(location).attr('href'),
                                payload: payload
                        });
                        $rootScope.shareDialog = false;
                    };
                }
            };
        });

		
        app.directive('palette', function($log, $rootScope, editorState) {
            return {
                restrict: 'E',
                replace: true,
                templateUrl: bridge.getUrl('sf/ng-templates/palette.html'),
                scope: {},
                controller: function($scope, $log, Artifacts, $element) {
                    $scope.BRUSH_WIDTHS = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
                    $scope.BRUSH_COLORS = ['#000000', '#808080', '#c0c0c0', '#ffffff', '#ff0000', '#800000', '#800080', '#ff00ff',
                    '#008000', '#00ff00', '#808000', '#ffff00', '#000080', '#0000ff', '#008080', '#00ffff'];
					
                    $scope.show = false;
                    $scope.brushWidth = 1;
                    $scope.brushColor = '#000000';
					
                    var active;
                    var ctx;
					
					$scope.drag = function () {
						console.log($($element[0]).draggable());
					}
					
                    $scope.setBrushColor = function (c) {
						editorState.brushColor = c;
                    };
					
                    $scope.setBrushWidth = function(w) {
						editorState.brushWidth = w;
                    };
					
                    $scope.save = function() {
                        Artifacts.save();
                    };
					
					$scope.del = function() {
						Artifacts.remove(editorState.activeArtifact.name);
					};
					
                    $scope.undo = function() {
						editorState.activeArtifact.undo();
                    };
                    $scope.share = function(){
                        $rootScope.shareDialog = !$rootScope.shareDialog;
                    };
					
					$scope.redo = function() {
						editorState.activeArtifact.redo();
					};
					
					$scope.setTool = function (tool) {
						editorState.tool = tool;
					};
					
					//Set active artifact
                    $scope.$on('active-artifact', function(e, args) {
                        a = args.artifact;
                        active = a;
                    });
                }
            };
        });
		
        angular.bootstrap(root, ['sf']);
    }

    $.get(bridge.getUrl('sf/ng-templates/content.html'), function(data) {
        initAngular(data);
    });

}(jQuery));
